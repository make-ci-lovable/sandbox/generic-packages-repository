# Generic Packages Repository

- Documentation: 
  - https://docs.gitlab.com/ee/user/packages/generic_packages/
  - https://docs.gitlab.com/ee/administration/packages/
- Issues / Epics:
  - https://gitlab.com/gitlab-org/gitlab/-/issues/277160
- Sample: https://gitlab.com/guided-explorations/cfg-data/write-ci-cd-variables-in-pipeline/-/tree/master

